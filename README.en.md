# go-light

#### Description
    
    web business framework based on go language (refer to the table below for specific functions)

|  序号   |         功能点          |              注意事项              |
|:-----:|:--------------------:|:------------------------------:|
|  one  |         Route matching         |      Support '' and ':' wildcards        |
|  two  |         Parameter binding         | uriquerybodyformheader is supported |
| three |         Parameter check         |       Mandatory check is provided by default, and user-defined check functions are supported       |
| four  |        Access static resources        |        You can configure the access and proxy directories yourself         |
| five  |        Binding middleware         | Support multiple sets of binding, the priority of global > local level depends on the order of binding triggered |
|  six  |        Built-in logging        |     Logs display colors based on different requests or responses      |
| seven |        Generating scaffold         |             Refer to the instructions             |
| eight | Support for custom error codes (markdown parsing) |          It has not been completed, and subsequent iterations will follow           |


#### Software Architecture

     |-- go-light
        |-- .gitignore      #git ignores files
        |-- img.png         #Gift code
        |-- go.mod          #Project dependency
        |-- go.sum
        |-- LICENSE         #Open source protocol
        |-- README.en.md    #Documentation (English)
        |-- README.md       #Description Document (Chinese)
        |-- cmd             #A compilable package
            |-- generate        #Scaffolding generator 
        |-- internal        #Core source code
            |-- pkg             #Public files within the project
                |-- global              #Global constants and enums
                |-- utils               #tool kit
            |-- scaffold        #go-light web framework scaffolding related core code
                |-- templates           #Scaffold form
                |-- generate.go         #Template generator core code
            |-- web             #go-light web framework related core code
                |-- binding             #Request parameter binding && Responder mapping && Request a parameter validator
                |-- controller.go       #Routing controller
                |-- engine.go           #Frame core engine
        |-- pkg             #Publicly accessible file
            |-- middleware      #Common middleware
            |-- server          #go-light The initiator of the web framework
            |-- utils           #Commonly used tools
        |-- test            #Test case   


#### Installation

    1.  The windows operating system is ready    

    2.  go version: 1.18

    3.  Configure the go environment（Including agency GOPROXY=https://goproxy.cn,direct）

    4.  Installation dependency: go get -u gitee.com/lstack_light/go-light


#### Instructions

    1.  Create your own project，execute (go mod init Own project name) After that, open the command line

    2.  go get -u gitee.com/lstack_light/go-light/cmd/generate
    
    3.  go install gitee.com/lstack_light/go-light/cmd/generate

    4.  generate -project "Project name" -name "File name" (You can view the help command with generate help)

    5.  You can use main.go until the console log reads "Scaffolding completed"


#### Technical support

    Contact author：1343450971@qq.com


#### Send gifts

    Want to support the author output better new content, you can ask the author to drink a cup of Luckin, thank you ~
![image](img.png)