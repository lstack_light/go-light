package test

import (
	"gitee.com/lstack_light/go-light/pkg/utils"
	"log"
	"os"
	"strings"
	"testing"
	"text/template"
)

/**
  @author: light
  @since: 2023/9/3
  @desc: 测试类
*/

func TestCreateTemplates(t *testing.T) {
	err := createTemplates(
		"model/model.go.tmpl",
		"service/service.go.tmpl",
		"service/impl/serviceImpl.go.tmpl",
		"controller/controller.go.tmpl",
		"cmd/main.go.tmpl",
	)
	if nil != err {
		panic(err)
	}
}

func createTemplates(modelFile, serviceInterfaceFile, serviceImplFile, routerFile, mainFile string) error {
	name := strings.ReplaceAll(strings.Title("user"), " ", "")
	model := "gitee.com/lstack_light/go-light"
	modelData := []byte("package bto\n\ntype {{.}} struct {\n\tId   int    `uri:\"id\"`\n\tName string `json:\"name\"`\n}")
	err := os.WriteFile(modelFile, modelData, 0644)
	if err != nil {
		log.Fatal("创建实体模板文件失败：", err)
		return err
	}
	err = generateModel(modelFile, name)
	if err != nil {
		log.Fatal("创建实体文件失败：", err)
		return err
	}
	serviceInterfaceData := []byte("package service\n\nimport (\n\t\"{{.Module}}/test/model/bto\"\n\t\"gitee.com/lstack_light/go-light/pkg/server/respData\"\n)\n\ntype {{.Name}}Service interface{\n\t{{range.Methods}}\n\t{{.Name}}({{.Params}}) *respData.Response\n\t{{end}}\n}")
	err = os.WriteFile(serviceInterfaceFile, serviceInterfaceData, 0644)
	if err != nil {
		log.Fatal("创建业务层接口模板文件失败：", err)
		return err
	}
	err = generateService(serviceInterfaceFile, model, name)
	if err != nil {
		log.Fatal("创建业务层接口文件失败：", err)
		return err
	}
	serviceImplData := []byte("package impl\n\nimport (\n    \"gitee.com/lstack_light/go-light/pkg/server/respData\"\n    \"strconv\"\n    \"{{.Module}}/test/model/bto\"\n)\n\nvar (\n\t{{.Name}}ServiceImpl = &{{.Self}}{}\n)\n\ntype {{.Self}} struct {\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) FindAll() *respData.Response {\n\tresult := struct {\n\t\tItems []bto.{{.Name}} `json:\"items\"`\n\t\tTotal int             `json:\"total\"`\n\t}{\n\t\tItems: []bto.{{.Name}}{},\n\t\tTotal: 0,\n\t}\n\treturn respData.SuccessResponse(result, \"操作成功\")\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) FindOne(param *bto.{{.Name}}) *respData.Response {\n\treturn respData.SuccessResponse(param, \"操作成功\")\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) CreateOne(param *bto.{{.Name}}) *respData.Response {\n\treturn respData.SuccessResponse(param, \"操作成功\")\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) UpdateOne(param *bto.{{.Name}}) *respData.Response {\n\treturn respData.SuccessResponse(param, \"操作成功\")\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) DeleteOne(param *bto.{{.Name}}) *respData.Response {\n\treturn respData.SuccessResponse(\"已删除：\"+strconv.Itoa(param.Id), \"操作成功\")\n}")
	err = os.WriteFile(serviceImplFile, serviceImplData, 0644)
	if err != nil {
		log.Fatal("创建业务层实现模板文件失败：", err)
		return err
	}
	err = generateServiceImpl(serviceImplFile, model, name)
	if err != nil {
		log.Fatal("创建业务层实现文件失败：", err)
		return err
	}
	routerData := []byte("package controller\n\nimport (\n\t\"gitee.com/lstack_light/go-light/pkg/middleware\"\n\t\"gitee.com/lstack_light/go-light/pkg/server\"\n\t\"{{.Module}}/test/service\"\n\t\"{{.Module}}/test/service/impl\"\n)\n\ntype {{.Self}} struct {\n\tservice.{{.Name}}Service\n}\n\nvar {{.Name}}Router = server.NewController(\"/api/users\", &{{.Self}}{\n\t{{.Name}}Service: impl.{{.Name}}ServiceImpl,\n}, middleware.Logger(true))\n\nfunc ({{.SelfPrefix}} *{{.Self}}) FindAll() {\n\t{{.Name}}Router.Get(\"\", {{.SelfPrefix}}.{{.Name}}Service.FindAll)\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) FindOne() {\n\t{{.Name}}Router.Get(\"/:id\", {{.SelfPrefix}}.{{.Name}}Service.FindOne)\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) CreateOne() {\n\t{{.Name}}Router.Post(\"\", {{.SelfPrefix}}.{{.Name}}Service.CreateOne)\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) UpdateOne() {\n\t{{.Name}}Router.Put(\"/:id\", {{.SelfPrefix}}.{{.Name}}Service.UpdateOne)\n}\n\nfunc ({{.SelfPrefix}} *{{.Self}}) DeleteOne() {\n\t{{.Name}}Router.Delete(\"/:id\", {{.SelfPrefix}}.{{.Name}}Service.DeleteOne)\n}")
	err = os.WriteFile(routerFile, routerData, 0644)
	if err != nil {
		log.Fatal("创建路由层模板文件失败：", err)
		return err
	}
	err = generateRouter(routerFile, model, name)
	if err != nil {
		log.Fatal("创建路由层文件失败：", err)
		return err
	}
	mainData := []byte("package test\n\nimport (\n\t\"context\"\n\t\"fmt\"\n\t\"{{.Module}}/test/controller\"\n\tlight \"gitee.com/lstack_light/go-light/pkg/server\"\n\t\"log\"\n\t\"net/http\"\n\t\"testing\"\n)\n\nfunc TestLight(t *testing.T) {\n\tctx, cancel := context.WithCancel(context.Background())\n\tengine := light.InitEngine().\n\t\tSetControllers(controller.{{.Name}}Router)\n\tserver := http.Server{\n\t\tAddr:    fmt.Sprintf(\":%d\", 9090),\n\t\tHandler: engine,\n\t}\n\tdefer func() {\n\t\tif err := recover(); nil != err {\n\t\t\tlog.Println(\"异常--\", err)\n\t\t\tcancel()\n\t\t\terr = server.Shutdown(ctx)\n\t\t\tif nil != err {\n\t\t\t\tlog.Fatal(\"关闭server异常\")\n\t\t\t}\n\t\t}\n\t}()\n\terr := server.ListenAndServe()\n\tif nil != err {\n\t\tpanic(\"启动服务异常：\" + err.Error())\n\t}\n}")
	err = os.WriteFile(mainFile, mainData, 0644)
	if err != nil {
		log.Fatal("创建主函数模板文件失败：", err)
		return err
	}
	err = generateMain(mainFile, model, name)
	if err != nil {
		log.Fatal("创建主函数文件失败：", err)
		return err
	}
	log.Println("已完成模板的创建")
	return nil
}

func generateModel(templateFile, name string) error {
	file, createErr := os.Create("model/bto/user.go")
	if createErr != nil {
		log.Fatal(createErr)
		return createErr
	}
	// 读取模板文件
	tmpl, tempErr := template.ParseFiles(templateFile)
	if tempErr != nil {
		log.Fatal("解析实体模板失败：", tempErr)
		return tempErr
	}
	// 执行模板填充并写入输出文件
	err := tmpl.Execute(file, name)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}
func generateService(templateName, module, name string) error {
	file, createErr := os.Create("service/user.go")
	if createErr != nil {
		log.Fatal(createErr)
		return createErr
	}
	// 读取模板文件
	tmpl, tempErr := template.ParseFiles(templateName)
	if tempErr != nil {
		log.Fatal("解析业务层接口模板失败：", tempErr)
		return tempErr
	}
	type Method struct {
		Name   string
		Params string
	}
	type Temp struct {
		Module  string
		Name    string
		Methods []Method
	}
	// 定义方法列表
	methods := []Method{
		{Name: "FindAll"},
		{Name: "FindOne", Params: "param *bto." + name},
		{Name: "CreateOne", Params: "param *bto." + name},
		{Name: "UpdateOne", Params: "param *bto." + name},
		{Name: "DeleteOne", Params: "param *bto." + name},
	}
	temp := Temp{
		Methods: methods,
		Name:    name,
		Module:  module,
	}
	// 执行模板填充并写入输出文件
	err := tmpl.Execute(file, temp)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func generateServiceImpl(templateName, module, name string) error {
	file, createErr := os.Create("service/impl/user.go")
	if createErr != nil {
		log.Fatal(createErr)
		return createErr
	}
	// 读取模板文件
	tmpl, tempErr := template.ParseFiles(templateName)
	if tempErr != nil {
		log.Fatal("解析业务层实现模板失败：", tempErr)
		return tempErr
	}
	type Temp struct {
		Name       string
		Module     string
		Self       string
		SelfPrefix string
	}
	temp := Temp{
		Name:       name,
		Module:     module,
		Self:       strings.ToLower(name) + "ServiceImpl",
		SelfPrefix: strings.ToLower(name)[0:1],
	}
	// 执行模板填充并写入输出文件
	err := tmpl.Execute(file, temp)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func generateRouter(templateName, module, name string) error {
	file, createErr := os.Create("controller/user.go")
	if createErr != nil {
		log.Fatal(createErr)
		return createErr
	}
	// 读取模板文件
	tmpl, tempErr := template.ParseFiles(templateName)
	if tempErr != nil {
		log.Fatal("解析路由层模板失败：", tempErr)
		return tempErr
	}
	// 定义引用变量
	type Temp struct {
		Name       string
		Module     string
		Self       string
		SelfPrefix string
	}
	temp := Temp{
		Name:       name,
		Module:     module,
		Self:       name + "Controller",
		SelfPrefix: strings.ToLower(name)[0:1],
	}
	// 执行模板填充并写入输出文件
	err := tmpl.Execute(file, temp)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func generateMain(templateName, module, name string) error {
	file, createErr := os.Create("cmd/main_test.go")
	if createErr != nil {
		log.Fatal(createErr)
		return createErr
	}
	// 读取模板文件
	tmpl, tempErr := template.ParseFiles(templateName)
	if tempErr != nil {
		log.Fatal("解析主函数模板失败：", tempErr)
		return tempErr
	}
	// 定义引用变量
	type Temp struct {
		Name   string
		Module string
	}
	temp := Temp{
		Name:   name,
		Module: module,
	}
	// 执行模板填充并写入输出文件
	err := tmpl.Execute(file, temp)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func TestName(t *testing.T) {
	// 获取当前程序运行的路径
	dir, err := os.Getwd()
	if err != nil {
		t.Fatal("无法获取当前程序路径：", err)
		return
	}
	// 更新依赖
	command := &utils.Command{
		Order: "go get -u gitee.com/lstack_light/go-light",
		Dir:   dir,
	}
	result, execErr := command.Exec()
	if nil != execErr || (!strings.Contains(result, "go: upgraded") && 0 < len(result)) {
		t.Fatal("更新 go-light 依赖失败：", result)
		return
	}
	t.Log(result)
}
