package impl

import (
	"gitee.com/lstack_light/go-light/pkg/server/respData"
	"gitee.com/lstack_light/go-light/test/model/bto"
	"strconv"
)

var (
	UserServiceImpl = &userServiceImpl{}
)

type userServiceImpl struct {
}

func (u *userServiceImpl) FindAll() *respData.Response {
	result := struct {
		Items []bto.User `json:"items"`
		Total int        `json:"total"`
	}{
		Items: []bto.User{},
		Total: 0,
	}
	return respData.SuccessResponse(result, "操作成功")
}

func (u *userServiceImpl) FindOne(param *bto.User) *respData.Response {
	return respData.SuccessResponse(param, "操作成功")
}

func (u *userServiceImpl) CreateOne(param *bto.User) *respData.Response {
	return respData.SuccessResponse(param, "操作成功")
}

func (u *userServiceImpl) UpdateOne(param *bto.User) *respData.Response {
	return respData.SuccessResponse(param, "操作成功")
}

func (u *userServiceImpl) DeleteOne(param *bto.User) *respData.Response {
	return respData.SuccessResponse("已删除："+strconv.Itoa(param.Id), "操作成功")
}
