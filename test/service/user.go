package service

import (
	"gitee.com/lstack_light/go-light/pkg/server/respData"
	"gitee.com/lstack_light/go-light/test/model/bto"
)

type UserService interface {
	FindAll() *respData.Response

	FindOne(param *bto.User) *respData.Response

	CreateOne(param *bto.User) *respData.Response

	UpdateOne(param *bto.User) *respData.Response

	DeleteOne(param *bto.User) *respData.Response
}
