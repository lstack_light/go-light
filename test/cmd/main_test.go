package test

import (
	"context"
	"fmt"
	light "gitee.com/lstack_light/go-light/pkg/server"
	"gitee.com/lstack_light/go-light/test/controller"
	"log"
	"net/http"
	"testing"
)

func TestLight(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	engine := light.InitEngine().
		SetControllers(controller.UserRouter)
	server := http.Server{
		Addr:    fmt.Sprintf(":%d", 9090),
		Handler: engine,
	}
	defer func() {
		if err := recover(); nil != err {
			log.Println("异常--", err)
			cancel()
			err = server.Shutdown(ctx)
			if nil != err {
				log.Fatal("关闭server异常")
			}
		}
	}()
	err := server.ListenAndServe()
	if nil != err {
		panic("启动服务异常：" + err.Error())
	}
}
