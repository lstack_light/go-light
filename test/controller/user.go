package controller

import (
	"gitee.com/lstack_light/go-light/pkg/middleware"
	"gitee.com/lstack_light/go-light/pkg/server"
	"gitee.com/lstack_light/go-light/test/service"
	"gitee.com/lstack_light/go-light/test/service/impl"
)

type UserController struct {
	service.UserService
}

var UserRouter = server.NewController("/api/users", &UserController{
	UserService: impl.UserServiceImpl,
}, middleware.Logger(true))

func (u *UserController) FindAll() {
	UserRouter.Get("", u.UserService.FindAll)
}

func (u *UserController) FindOne() {
	UserRouter.Get("/:id", u.UserService.FindOne)
}

func (u *UserController) CreateOne() {
	UserRouter.Post("", u.UserService.CreateOne)
}

func (u *UserController) UpdateOne() {
	UserRouter.Put("/:id", u.UserService.UpdateOne)
}

func (u *UserController) DeleteOne() {
	UserRouter.Delete("/:id", u.UserService.DeleteOne)
}
