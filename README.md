# go-light

#### 介绍

    基于go语言实现的web业务框架（具体功能参考下面表格）

| 序号  |         功能点          |              注意事项              |
|:---:|:--------------------:|:------------------------------:|
|  1  |         路由匹配         |       支持 '*' 以及 ':' 通配符        |
|  2  |         参数绑定         | 支持uri/query/body/form/header类型 |
|  3  |         参数校验         |       默认提供必填校验,支持自定义校验函数       |
|  4  |        访问静态资源        |        可以自己配置访问以及代理的目录         |
|  5  |        绑定中间件         | 支持绑定多套,优先级 全局>局部 同等级就依赖绑定的顺序触发 |
|  6  |        内置日志功能        |     日志根据不同请求或者不同响应展示对应的颜色      |
|  7  |        生成脚手架         |             参考使用说明             |
|  8  | 支持自定义错误码（markdown解析） |          暂未完成，后续迭代跟进           |


#### 软件架构
    
     |-- go-light
        |-- .gitignore      #git忽略的文件
        |-- img.png         #打赏码
        |-- go.mod          #项目依赖
        |-- go.sum
        |-- LICENSE         #开源协议
        |-- README.en.md    #说明文档（英文）
        |-- README.md       #说明文档（中文）
        |-- cmd             #可编译的程序包
            |-- generate        #脚手架生成器 
        |-- internal        #核心源码
            |-- pkg             #项目内公共文件
                |-- global              #全局常量和枚举
                |-- utils               #工具包
            |-- scaffold        #go-light web框架的脚手架相关核心代码
                |-- templates           #脚手架的模板
                |-- generate.go         #模板生成器核心代码
            |-- web             #go-light web框架的相关核心代码
                |-- binding             #请求参数绑定 && 响应体映射 && 请求参数校验器
                |-- controller.go       #路由控制器
                |-- engine.go           #框架核心引擎
        |-- pkg             #公共可访问文件
            |-- middleware      #常用的中间件
            |-- server          #go-light web框架的启动器
            |-- utils           #常用的工具
        |-- test            #测试用例    

#### 安装教程

    1.  准备 windows 操作系统    

    2.  go version: 1.18

    3.  配置go的环境（包括代理 GOPROXY=https://goproxy.cn,direct）

    4.  安装依赖: go get -u gitee.com/lstack_light/go-light
    
#### 使用说明
        
    1.  创建自己的项目，执行 (go mod init 自己的项目名称) 后，打开命令行

    2.  go get -u gitee.com/lstack_light/go-light/cmd/generate
    
    3.  go install gitee.com/lstack_light/go-light/cmd/generate

    4.  generate -project "项目名称" -name "文件名称" (可以通过 generate help 查看帮助命令)

    5.  等待控制台日志显示 "已搭建完成脚手架" 字样即可使用 main.go


#### 技术支持

    联系作者：1343450971@qq.com


#### 打赏

    想要支持作者输出更好的新内容，可以请作者喝一杯瑞幸，谢谢~
![image](img.png)