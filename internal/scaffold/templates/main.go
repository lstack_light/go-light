package templates

import (
	_const "gitee.com/lstack_light/go-light/internal/pkg/global"
	"log"
	"os"
	"text/template"
)

/**
  @author: light
  @since: 2023/9/3
  @desc: 主函数模板
*/

var _ Template = &Main{}

type Main struct {
	Receiver *Receiver
	TemplateParams
}

func (m *Main) Execute(eventType _const.TemplateEventType) error {
	switch eventType {
	case _const.InitEvent:
		return m.Init()
	case _const.GenerateEvent:
		return m.Generate()
	}
	return nil
}
func (m *Main) Init() error {
	mainData := []byte("package main\n\nimport (\n\t\"context\"\n\t\"fmt\"\n\t\"{{.Module}}/internal/controller\"\n\tlight \"gitee.com/lstack_light/go-light/pkg/server\"\n\t\"log\"\n\t\"net/http\"\n)\n\nfunc main() {\n\tctx, cancel := context.WithCancel(context.Background())\n\tengine := light.InitEngine().\n\t\tSetControllers(controller.{{.Name}}Router)\n\tserver := http.Server{\n\t\tAddr:    fmt.Sprintf(\":%d\", 9090),\n\t\tHandler: engine,\n\t}\n\tdefer func() {\n\t\tif err := recover(); nil != err {\n\t\t\tlog.Println(\"异常--\", err)\n\t\t\tcancel()\n\t\t\terr = server.Shutdown(ctx)\n\t\t\tif nil != err {\n\t\t\t\tlog.Fatal(\"关闭server异常\")\n\t\t\t}\n\t\t}\n\t}()\n\terr := server.ListenAndServe()\n\tif nil != err {\n\t\tpanic(\"启动服务异常：\" + err.Error())\n\t}\n}")
	err := os.WriteFile(m.TemplateName, mainData, 0644)
	if err != nil {
		log.Fatal("创建主函数模板文件失败：", err)
		return err
	}
	return nil
}

func (m *Main) Generate() error {
	file, createErr := os.Create(m.OutputFile)
	if createErr != nil {
		log.Fatal(createErr)
		return createErr
	}
	// 读取模板文件
	tmpl, tempErr := template.ParseFiles(m.TemplateName)
	if tempErr != nil {
		log.Fatal("解析主函数模板失败：", tempErr)
		return tempErr
	}
	// 定义引用变量
	type Temp struct {
		Name   string
		Module string
	}
	temp := Temp{
		Name:   m.Name,
		Module: m.Module,
	}
	// 执行模板填充并写入输出文件
	err := tmpl.Execute(file, temp)
	if err != nil {
		log.Fatal(err)
		return err
	}
	// 删除模板文件
	_ = os.RemoveAll(m.TemplateName)
	log.Println("Go文件已生成：", m.OutputFile)
	return nil
}
