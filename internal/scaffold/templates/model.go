package templates

import (
	"errors"
	_const "gitee.com/lstack_light/go-light/internal/pkg/global"
	"log"
	"os"
	"text/template"
)

/**
  @author: light
  @since: 2023/9/3
  @desc: 实体模板
*/

var _ Template = &Model{}

type Model struct {
	Receiver *Receiver
	TemplateParams
}

func (m *Model) Execute(eventType _const.TemplateEventType) error {
	if _const.InitEvent != eventType {
		err := errors.New("执行的事件不是初始化模板,拒绝执行！！！")
		log.Fatal(err)
		return err
	}
	err := m.Init()
	if nil != err {
		log.Fatal(err)
		return err
	}
	return m.Generate()
}

func (m *Model) Init() error {
	modelData := []byte("package bto\n\ntype {{.}} struct {\n\tId   int    `uri:\"id\"`\n\tName string `json:\"name\"`\n}")
	err := os.WriteFile(m.TemplateName, modelData, 0644)
	if err != nil {
		log.Fatal("创建实体模板文件失败：", err)
		return err
	}
	return m.Receiver.next.Execute(_const.InitEvent)
}

func (m *Model) Generate() error {
	file, createErr := os.Create(m.OutputFile)
	if createErr != nil {
		log.Fatal(createErr)
		return createErr
	}
	// 读取模板文件
	tmpl, tempErr := template.ParseFiles(m.TemplateName)
	if tempErr != nil {
		log.Fatal("解析实体模板失败：", tempErr)
		return tempErr
	}
	// 执行模板填充并写入输出文件
	err := tmpl.Execute(file, m.Name)
	if err != nil {
		log.Fatal(err)
		return err
	}
	// 删除模板文件
	_ = os.RemoveAll(m.TemplateName)
	log.Println("Go文件已生成：", m.OutputFile)
	return m.Receiver.next.Execute(_const.GenerateEvent)
}
