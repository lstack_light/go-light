package templates

import (
	_const "gitee.com/lstack_light/go-light/internal/pkg/global"
	"log"
)

/**
  @author: light
  @since: 2023/9/3
  @desc: 模板
*/

type TemplateParams struct {
	OutputFile   string
	TemplateName string
	Module       string
	Name         string
}

type Template interface {
	Init() error
	Generate() error
}

// Command 命令接口
type Command interface {
	Execute(eventType _const.TemplateEventType) error
}

// Receiver 接收者
type Receiver struct {
	// 下一个执行链
	next Command
	Template
}

func (r *Receiver) SetNext(next Command) {
	r.next = next
}

// Invoker 调用者
type Invoker struct {
	command Command
}

func (i *Invoker) SetCommand(command Command) {
	i.command = command
}

func (i *Invoker) ExecuteCommand(eventType _const.TemplateEventType) {
	if err := i.command.Execute(eventType); nil != err {
		log.Fatal(err)
	}
}
