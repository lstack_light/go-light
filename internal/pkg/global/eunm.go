package global

/**
  @author: light
  @since: 2023/9/3
  @desc: 枚举
*/

type TemplateEventType string

const (
	InitEvent     TemplateEventType = "Init"
	GenerateEvent TemplateEventType = "Generate"
)
