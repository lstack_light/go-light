package global

/**
  @author: light
  @since: 2023/8/5
  @desc: 常量
*/

const (
	QuestionMarkWildcard      = "?"
	StarMarkWildcard          = "*"
	StarMarkWildcardCharacter = '*'
	TheColonWildcardCharacter = ':'
	TheSlashSeparator         = "/"
)

const AcceptLanguage = "Accept-Language"
