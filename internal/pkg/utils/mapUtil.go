package utils

/**
  @author: light
  @since: 2023/8/5
  @desc: map相关工具
*/

type Map map[string]interface{}

func (m *Map) Get(key string) interface{} {
	return (*m)[key]
}
func (m *Map) Delete(key string) {
	delete(*m, key)
}
