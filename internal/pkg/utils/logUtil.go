package utils

import (
	"github.com/logrusorgru/aurora"
	"log"
	"net/http"
	"os"
	"sync"
)

/**
  @author: light
  @since: 2023/6/4
  @desc: 日志工具
*/

var (
	once   sync.Once
	logger *log.Logger
)

func GetLogger() *log.Logger {
	once.Do(func() {
		if nil == logger {
			// 将log输出到标准输出，并设置前缀和日志级别
			logger = log.New(os.Stdout, "[LIGHT]\t", log.Ldate|log.Ltime)
		}
	})
	return logger
}

func BeautifyRequestMode(method string) interface{} {
	var style interface{}
	switch method {
	case http.MethodGet:
		style = aurora.BgBlue(method)
	case http.MethodPost:
		style = aurora.BgBrightMagenta(method)
	case http.MethodPut:
		style = aurora.BgYellow(method)
	case http.MethodDelete:
		style = aurora.BgRed(method)
	default:
		style = aurora.BgWhite(method)
	}
	return style
}

func BeautifyResponseCode(code int) interface{} {
	var style interface{}
	switch code {
	case http.StatusOK:
		style = aurora.BgGreen(code)
	case http.StatusNotFound:
		style = aurora.BgWhite(code)
	case http.StatusInternalServerError:
		style = aurora.BgRed(code)
	default:
		style = aurora.BgBlack(code)
	}
	return style
}
