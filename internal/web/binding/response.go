package binding

/**
  @author: light
  @since: 2023/4/23
  @desc: 响应格式
*/

type BaseResponse struct {
	HttpStatusCode int `json:"-"`
	File           *struct {
		Name    string
		Content []byte
	} `json:"-"`
}
