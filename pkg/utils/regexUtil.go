package utils

import "regexp"

/**
  @author: light
  @since: 2023/8/5
  @desc: 正则相关工具
*/

func RegexMatch(pattern, source string) bool {
	regex := regexp.MustCompile(pattern)
	if regex.MatchString(source) {
		return true
	}
	return false
}
