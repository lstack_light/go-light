package respData

import (
	"gitee.com/lstack_light/go-light/internal/web/binding"
	"net/http"
)

/**
  @author: light
  @since: 2023/4/23
  @desc: 响应格式
*/

type Response struct {
	BusinessStatusCode   int         `json:"statusCode"`
	Message              string      `json:"message"`
	Data                 interface{} `json:"data"`
	binding.BaseResponse `json:"-"`
}

func StaticFileResponse(fileName string, Content []byte) *Response {
	return &Response{
		BaseResponse: struct {
			HttpStatusCode int `json:"-"`
			File           *struct {
				Name    string
				Content []byte
			} `json:"-"`
		}{
			File: &struct {
				Name    string
				Content []byte
			}{
				Name:    fileName,
				Content: Content,
			},
		},
	}
}

func SystemErrorResponse(message string) *Response {
	return &Response{
		BaseResponse: struct {
			HttpStatusCode int `json:"-"`
			File           *struct {
				Name    string
				Content []byte
			} `json:"-"`
		}{
			HttpStatusCode: http.StatusInternalServerError,
		},
		BusinessStatusCode: http.StatusInternalServerError,
		Message:            message,
	}
}
func ErrorResponse(statusCode int, message string) *Response {
	return &Response{
		BaseResponse: struct {
			HttpStatusCode int `json:"-"`
			File           *struct {
				Name    string
				Content []byte
			} `json:"-"`
		}{
			HttpStatusCode: http.StatusOK,
		},
		BusinessStatusCode: statusCode,
		Message:            message,
	}
}

func SuccessResponse(data interface{}, message ...string) *Response {
	msg := ""
	if 0 < len(message) {
		msg = message[0]
	}
	return &Response{
		BaseResponse: struct {
			HttpStatusCode int `json:"-"`
			File           *struct {
				Name    string
				Content []byte
			} `json:"-"`
		}{
			HttpStatusCode: http.StatusOK,
		},
		BusinessStatusCode: 0,
		Data:               data,
		Message:            msg,
	}
}
