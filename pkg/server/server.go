package server

import (
	"gitee.com/lstack_light/go-light/internal/web"
)

/**
  @author: light
  @since: 2023/9/3
  @desc: 服务
*/

func InitEngine() *web.Engine {
	return web.InitEngine()
}

func NewController(path string, module interface{}, middleWares ...*web.MiddleWare) *web.Controller {
	return web.NewController(path, module, middleWares...)
}
